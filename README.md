# Tracks your daily meals getting the grams from proteins, carbohydrates and
# fats; it also calculates the calories per meal and per day.