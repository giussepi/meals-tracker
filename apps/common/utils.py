# -*- coding: utf-8 -*-
""" common utils """

import random


def random_string_with_length(longitud=10):
    """
    returns a random string with a defined length
    """
    lista_letras_numeros = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNO\
PQRSTUVWXYZ1234567890"
    return "".join(
        [random.choice(lista_letras_numeros) for i in xrange(longitud)])
