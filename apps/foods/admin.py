# -*- coding: utf-8 -*-
""" foods Admin """

from django.contrib import admin
from modeltranslation.admin import TranslationAdmin, TranslationTabularInline

from .models import Food, Presentation


class PresentationInline(TranslationTabularInline):
    """ Inline admin model for Presentation model """
    model = Presentation


class FoodAdmin(TranslationAdmin):
    """ model admin that manages the foods """
    search_fields = ('name', )
    fields = ('name', 'grams', 'carbohydrates', 'protein', 'fat', 'calories')
    readonly_fields = ('calories', )
    inlines = [PresentationInline]


admin.site.register(Food, FoodAdmin)
