# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Food'
        db.create_table('foods_food', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('carbohydrates', self.gf('django.db.models.fields.FloatField')()),
            ('protein', self.gf('django.db.models.fields.FloatField')()),
            ('fat', self.gf('django.db.models.fields.FloatField')()),
            ('calories', self.gf('django.db.models.fields.FloatField')(null=True, blank=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=120)),
            ('grams', self.gf('django.db.models.fields.FloatField')(default=100)),
        ))
        db.send_create_signal('foods', ['Food'])


    def backwards(self, orm):
        # Deleting model 'Food'
        db.delete_table('foods_food')


    models = {
        'foods.food': {
            'Meta': {'object_name': 'Food'},
            'calories': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'carbohydrates': ('django.db.models.fields.FloatField', [], {}),
            'fat': ('django.db.models.fields.FloatField', [], {}),
            'grams': ('django.db.models.fields.FloatField', [], {'default': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '120'}),
            'protein': ('django.db.models.fields.FloatField', [], {})
        }
    }

    complete_apps = ['foods']