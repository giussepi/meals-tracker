# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Food.name_en'
        db.add_column(u'foods_food', 'name_en',
                      self.gf('django.db.models.fields.CharField')(max_length=120, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Food.name_es'
        db.add_column(u'foods_food', 'name_es',
                      self.gf('django.db.models.fields.CharField')(max_length=120, null=True, blank=True),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'Food.name_en'
        db.delete_column(u'foods_food', 'name_en')

        # Deleting field 'Food.name_es'
        db.delete_column(u'foods_food', 'name_es')


    models = {
        u'foods.food': {
            'Meta': {'ordering': "['name']", 'object_name': 'Food'},
            'calories': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'carbohydrates': ('django.db.models.fields.FloatField', [], {}),
            'fat': ('django.db.models.fields.FloatField', [], {}),
            'grams': ('django.db.models.fields.FloatField', [], {'default': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '120'}),
            'name_en': ('django.db.models.fields.CharField', [], {'max_length': '120', 'null': 'True', 'blank': 'True'}),
            'name_es': ('django.db.models.fields.CharField', [], {'max_length': '120', 'null': 'True', 'blank': 'True'}),
            'protein': ('django.db.models.fields.FloatField', [], {})
        },
        u'foods.presentation': {
            'Meta': {'object_name': 'Presentation'},
            'food': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['foods.Food']"}),
            'grams': ('django.db.models.fields.FloatField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '120'})
        }
    }

    complete_apps = ['foods']