# -*- coding: utf-8 -*-
""" foods models """

from django.db import models
from django.utils.translation import ugettext_lazy as _


class Base_Macronutrients(models.Model):
    """  abstract class that contains the base macronutrients fields """
    carbohydrates = models.FloatField(_('carbohydrates'))
    protein = models.FloatField(_('protein'))
    fat = models.FloatField(_('fat'))
    calories = models.FloatField(_('calories'), blank=True, null=True)

    class Meta:
        """ model meta options """
        abstract = True

    def calculate_calories(self):
        """
        returns the calories based on the quantity of carbohydrates, protein
        and fat
        """
        return self.carbohydrates * 4 + self.protein * 4 + self.fat * 9


class Food(Base_Macronutrients):
    """ stores food nutritional values """
    name = models.CharField(_('Name'), max_length=120)
    grams = models.FloatField(_('Grams'), default=100)

    class Meta:
        """ model meta options """
        ordering = ['name']
        verbose_name = _('Food')
        verbose_name_plural = _('Foods')

    def __unicode__(self):
        """ returns the object unicode representation """
        return u'%s' % self.name

    def save(self, *args, **kwargs):
        """ recalculates the calories before saving """
        self.calories = self.calculate_calories()
        super(Food, self).save(*args, **kwargs)

    def get_html_options_presentations(self):
        """ returns the html options presentations of the food """
        presentations = self.presentation_set.all()
        if presentations:
            options = "<option value=''>Select a presentation</option>"
            tpl = "<option value='{}'>{}</option>"
            for presentation in presentations:
                options += tpl.format(presentation.id, presentation.name)
        else:
            options = "<option value=''>---------</option>"
        return options


class Presentation(models.Model):
    """ stores the differents ways of cooking/serving each food """
    food = models.ForeignKey(Food)
    grams = models.FloatField()
    name = models.CharField(max_length=120)

    def __unicode__(self):
        """ returns the object unicode representation """
        return u'%s' % self.name
