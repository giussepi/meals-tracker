# -*- coding: utf-8 -*-
""" Food application tests """

from django.test import TestCase
from common.utils import random_string_with_length
from foods.models import Food
from random import randint


class BaseOperation(TestCase):
    """ basic operations """

    def _create_foods(self, num=10):
        """ creates the specified number of foods """
        for i in xrange(num):
            Food.objects.create(
                name=random_string_with_length(5), grams=randint(1, 100),
                carbohydrates=randint(1, 100), protein=randint(1, 100),
                fat=randint(1, 100))


class Calories_Food(BaseOperation):
    """ Food tests """

    def test_create_food(self):
        """ verifies that calories from new food are calculated correctly"""
        self._create_foods(1)
        food = Food.objects.get(id=1)
        self.assertTrue(
            food.calories != 0, 'The food calories were not calculated')
        self.assertTrue(food.calories == food.calculate_calories(),
                        'The food calories were not calculated correctly')

    def test_modify_food(self):
        """ verifies that calories are recalculated after updating a food """
        self._create_foods(1)
        food = Food.objects.get(id=1)
        initial_food_calories = food.calories
        food.carbohydrates += food.carbohydrates
        food.save()
        updated_food = Food.objects.get(id=1)
        self.assertTrue(initial_food_calories < updated_food.calories,
                        'Food calories were no updated')
