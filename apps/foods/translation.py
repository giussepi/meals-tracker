# -*- coding: utf-8 -*-
""" foods' translation file """

from modeltranslation.translator import translator, TranslationOptions

from .models import Food, Presentation


class FoodTranslationOptions(TranslationOptions):
    """ food's model translation  """
    fields = ('name', )


class PresentationTranslationOptions(TranslationOptions):
    """ presentation's model translation """
    fields = ('name', )


translator.register(Food, FoodTranslationOptions)
translator.register(Presentation, PresentationTranslationOptions)
