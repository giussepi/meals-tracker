# -*- coding: utf-8 -*-
""" foods' urls """

from django.conf.urls import patterns, url


urlpatterns = patterns(
    'foods.views',
    url(
        r'^json_presentations_by_food/(?P<food_id>\d+)/$',
        'json_presentations_by_food',
        name='foods.json_presentations_by_food'
    ),
)
