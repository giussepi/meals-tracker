# -*- coding: utf-8 -*-
""" foods' views """

import json

from django.http import HttpResponse, HttpResponseBadRequest
from django.shortcuts import get_object_or_404

from .models import Food


def json_presentations_by_food(request, food_id):
    """ returns all the meal's presentation in json format """
    if request.is_ajax() and request.method == 'GET':
        food = get_object_or_404(Food, pk=food_id)
        options = food.get_html_options_presentations()
        return HttpResponse(json.dumps({'options': options}))
    return HttpResponseBadRequest()
