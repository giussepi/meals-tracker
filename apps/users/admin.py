# -*- coding: utf-8 -*-
""" users' Admin """

from django.contrib import admin
from django.shortcuts import get_object_or_404, render_to_response
from django.template import RequestContext

from .forms import AdminDailyConsumeForm, AdminMealForm, \
    AdminUserProfileForm, AdminAddUserProfileForm, InlineFoodMealForm
from .models import UserProfile, Daily_Consume, Meal


class UserProfileAdmin(admin.ModelAdmin):
    """ admin model that manages the profiles """
    form = AdminUserProfileForm
    list_display = ('__unicode__', 'user')
    list_display_links = ('__unicode__', 'user')
    # fields = (
    #     'height', 'weight', 'physical_activity', 'goal', 'gender', 'birthday',
    #     'body_fat_percent', 'bmr', 'tee', 'rdi_carbohydrates', 'rdi_protein',
    #     'rdi_fat', 'calories', 'user'
    # )
    fieldsets = (
        ('Basic Information', {
            'fields': (('username', 'email'),
                       'user', ('first_name', 'last_name'),
                       ('gender', 'birthday'), ('height', 'weight'),
                       ('physical_activity', 'goal',),
                       ('body_fat_percent', 'bmr', 'tee',))
        }),
        ('RDI', {
            'fields': (
                'recalculate_rdis',
                ('rdi_protein', 'rdi_carbohydrates'),
                ('rdi_fat', 'calories')
            )
        }),
    )
    actions = ['delete_objects_selected']

    class Media:
        """ model admin media files """
        js = ("users/js/jquery-1.8.0.min.js",
              "users/js/highcharts/highcharts.js",
              "users/js/rdi_pie_chart.js",
              "users/js/userprofile_basic_info.js",
              )

    def queryset(self, request):
        """
        if the user is a superuser returns all the queryset, else
        returns the queryset with the objects that belong to the current user
        """
        queryset = super(self.__class__, self).queryset(request)
        if request.user.is_superuser:
            return queryset
        return queryset.filter(user=request.user)

    def add_view(self, request, form_url='', extra_context=None):
        """ custom addition view """
        form = AdminAddUserProfileForm()
        if request.method == 'POST':
            form = AdminAddUserProfileForm(request.POST)
            if form.is_valid():
                return form.save(request)
        return render_to_response(
            'admin/users/userprofile/add_form.html',
            {'form': form, 'app_label': 'users',
             'changelist': 'admin:users_userprofile_changelist',
             'verbose_name_plural': 'user profiles',
             'verbose_name': 'user profile'},
            context_instance=RequestContext(request))

    def change_view(self, request, object_id, form_url='', extra_context=None):
        """
        adds the user of the profile, the pie chart and the weekly chart as a
        extra_context
        """
        extra_context = extra_context or {}
        userprofile = get_object_or_404(UserProfile, id=object_id)
        extra_context['profile_user'] = userprofile.user
        extra_context['rdis'] = userprofile.get_rdis()
        latest_daily_consume = userprofile.get_latest_daily_consume()
        if latest_daily_consume:
            extra_context['weekly_chart'] = latest_daily_consume.weekly_chart()
        return super(UserProfileAdmin, self).change_view(
            request, object_id, form_url, extra_context=extra_context)

    def has_add_permission(self, request):
        """ restricts the userprofile addition to only superusers """
        if request.user.is_superuser:
            return True
        return False

    def has_delete_permission(self, request, obj=None):
        """ restricts the userprofile deletion to only superusers """
        if request.user.is_superuser:
            return True
        return False

    def get_actions(self, request):
        """ restricts the delete action to only superusers """
        actions = super(UserProfileAdmin, self).get_actions(request)
        del actions['delete_selected']
        if not request.user.is_superuser:
            del actions['delete_objects_selected']
        return actions

    def delete_objects_selected(self, request, queryset):
        """
        deletes the objects selected using its user to create a cascade
        deletion
        Avoids the deletion of the self user.
        """
        for obj in queryset.exclude(id=request.user.userprofile.id):
            obj.user.delete()
        self.message_user(request, 'The selected userprofiles were deleted.')
    delete_objects_selected.short_description = 'Delete selected user profiles'

    def delete_model(self, request, obj):
        """ deletes the User of the profile """
        obj.user.delete()


class Daily_ConsumeAdmin(admin.ModelAdmin):
    """ admin model that manages the daily consumes """
    date_hierarchy = 'date'
    form = AdminDailyConsumeForm

    class Media:
        """ model admin media files """
        css = {"all": ("users/css/admin/charts.css", )}

    def queryset(self, request):
        """
        returns the queryset with the objects that belong to the current user
        """
        return super(self.__class__, self).queryset(request).filter(
            user=request.user)

    def change_view(self, request, object_id, form_url='', extra_context=None):
        """ adds the pie chart as a extra_context """
        extra_context = extra_context or {}
        if object_id:
            consume = get_object_or_404(Daily_Consume, id=object_id)
            extra_context['chart'] = consume.data_total_intake_chart()
            extra_context['day_consume_chart'] = consume.day_consume_chart()
        return super(Daily_ConsumeAdmin, self).change_view(
            request, object_id, form_url, extra_context=extra_context)


class FoodsInline(admin.TabularInline):
    """ admin model that manages the foods """
    model = Meal.foods.through
    form = InlineFoodMealForm


class MealAdmin(admin.ModelAdmin):
    """ admin model that manages the meals """
    form = AdminMealForm
    inlines = [FoodsInline, ]

    class Media:
        """ model admin media files """
        css = {
            "all": (
                "users/css/admin/charts.css",
            )
        }

    def queryset(self, request):
        """
        returns the queryset with the objects that belong to the current user
        """
        return super(self.__class__, self).queryset(request).filter(
            daily_consume__user=request.user)

    def change_view(self, request, object_id, form_url='', extra_context=None):
        """ adds the pie chart as a extra_context """
        extra_context = extra_context or {}
        if object_id:
            meal = get_object_or_404(Meal, id=object_id)
            extra_context['chart'] = meal.daily_consume.\
                data_total_intake_chart()
            extra_context['meal_macronutrients'] = meal.\
                get_macronutrients_values()
        return super(MealAdmin, self).change_view(
            request, object_id, form_url, extra_context=extra_context)


admin.site.register(UserProfile, UserProfileAdmin)
admin.site.register(Daily_Consume, Daily_ConsumeAdmin)
admin.site.register(Meal, MealAdmin)
