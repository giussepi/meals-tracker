# -*- coding: utf-8 -*-
""" users' constants """

from django.utils.translation import ugettext_lazy as _


class BaseObject(object):
    """ holds basic operations """

    @staticmethod
    def get_value(option, options_container):
        """  """
        for obj_tuple in options_container:
            if obj_tuple[0] == option:
                return obj_tuple[1]
        return 0


class PhysicalActivity(BaseObject):
    """
    holds the physical activity's options and their corresponding
    protein's requirements and activity's factors
    """
    NOTHING = '0'
    LIGHT = '1'
    MODERATE = '2'
    MODERATE_DAILY = '3'
    HEAVY = '4'
    HEAVY_DAILY = '5'
    HEAVY_DAILY_PLUS_CARDIO = '6'

    CHOICES = (
        (NOTHING, _('No sports or training')),
        (LIGHT, _('Jogger or light fitness training')),
        (MODERATE, _('Moderate training, 3x per week')),
        (MODERATE_DAILY, _('Moderate daily weight training or aerobics')),
        (
            HEAVY,
            _('Intense weight training 3-4 days per week or other daily high '
            'volume exercise')
        ),
        (HEAVY_DAILY, _('Elite Athlete or weight training >5 days per week)')),
        (
            HEAVY_DAILY_PLUS_CARDIO,
             _('heavy weight training daily, plus cardio 3x per week')
        ),
    )

    # protein requirements g/kg for lean body mass
    # Xg/lb * 2.2046 = Yg/kg
    PROTEIN_REQUIREMENT = (
        (NOTHING, 1.1), # 0.5g/lb
        (LIGHT, 1.32), # 0.6g/lb
        (MODERATE, 1.54), # 0.7g/lb
        (MODERATE_DAILY, 1.76), # 0.8g/lb
        (HEAVY, 1.98 ), # 0.9g/lb
        (HEAVY_DAILY, 2.21), # 1g/lb
        (HEAVY_DAILY_PLUS_CARDIO, 3.31), # 1.5g/lb
    )

    ACTIVITY_FACTOR = (
        (NOTHING, 1.2),
        (LIGHT, 1.3),
        (MODERATE, 1.5),
        (MODERATE_DAILY, 1.6),
        (HEAVY, 1.7 ),
        (HEAVY_DAILY, 1.8),
        (HEAVY_DAILY_PLUS_CARDIO, 1.9),        
    )

    @staticmethod
    def get_protein_requirement(option):
        """ returns the protein's requirement of the option """
        return PhysicalActivity.get_value(
            option, PhysicalActivity.PROTEIN_REQUIREMENT)

    @staticmethod
    def get_activity_factor(option):
        """ returns the activity factor of the option """
        return PhysicalActivity.get_value(
            option, PhysicalActivity.ACTIVITY_FACTOR)


class Gender(object):
    """ holds the genders """
    MALE = 'M'
    FEMALE = 'F'

    CHOICES = ((MALE, _(u'Male')), (FEMALE, _(u'Female')))


class Goal(BaseObject):
    """ holds the user's goals and it's factors """
    LOSE = 'L'
    MAINTAIN = 'M'
    BUILD = 'B'

    CHOICES = (
        (LOSE, _(u'Lose Weight')),
        (MAINTAIN, _(u'Maintain Weight')),
        (BUILD, _(u'Build Muscle')),
    )

    # this factor is relative to the total calories need
    CALORIES_FACTOR = (
        (LOSE, 0.85),
        (MAINTAIN, 1),
        (BUILD, 1.15),
    )

    # this factor is relative to the daily grams of protein
    # ex: daily grams of fat = grams of protein * 0.21
    FAT_FACTOR = (
        (LOSE, 0.21),
        (MAINTAIN, 0.21),
        (BUILD, 0.42),
    )

    # this factor is relative to the quantity of required protein
    PROTEIN_FACTOR = (
        (LOSE, 1.09), # 1.5?
        (MAINTAIN, 1),
        (BUILD, 1),
    )

    # Xg/lb * 2.2046 = Yg/kg
    CARBS_FACTOR = (
        (LOSE, 1.93), # 0.75g/lb - 1g/lb // using an average
        (MAINTAIN, 2.89),# it's the average between lose and build
        (BUILD, 3.85), # 1.5g/lb - 2g/lb // using an average
    )    

    @staticmethod
    def get_calories_factor(option):
        """ returns the calories' factor of the option """
        return Goal.get_value(option, Goal.CALORIES_FACTOR)

    @staticmethod
    def get_fat_factor(option):
        """ returns the calories' factor of the option """
        return Goal.get_value(option, Goal.FAT_FACTOR)

    @staticmethod
    def get_protein_factor(option):
        """ returns the protein's factor of the option """
        return Goal.get_value(option, Goal.PROTEIN_FACTOR)

    @staticmethod
    def get_carbs_factor(option):
        """ returns the carbs's factor of the option """
        return Goal.get_value(option, Goal.CARBS_FACTOR)
