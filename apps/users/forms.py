# -*- coding:utf-8 -*-
""" Daily Consume Forms """

from datetime import date

from django import forms
from django.contrib.admin import widgets
from django.contrib.auth.models import User
from django.contrib import messages
from django.core.exceptions import ObjectDoesNotExist
from django.db import transaction
from django.shortcuts import redirect
from django.utils.translation import ugettext_lazy as _

from common.fields import UsernameField
from .models import Daily_Consume, Meal, UserProfile, MealFood


class AdminDailyConsumeForm(forms.ModelForm):
    """ Daily Consume form for the admin interface """
    class Meta:
        """ form meta options """
        model = Daily_Consume

    class Media:
        """ form media files """
        css = {'all': ('common/css/admin/admin.css', )}
        js = ('users/js/jquery-1.8.0.min.js',
              'users/js/highcharts/highcharts.js',
              'users/js/draw_charts.js',
              'common/js/hide_user.js')

    # user = forms.IntegerField(widget=forms.HiddenInput)

    def __init__(self, *args, **kwargs):
        """ customizing"""
        super(AdminDailyConsumeForm, self).__init__(*args, **kwargs)
        self.fields['user'].widget = forms.HiddenInput()

    # def clean_user(self):
    #     """ validates that the user exists and returns the User object """
    #     try:
    #         user = User.objects.get(id=self.cleaned_data['user'])
    #     except ObjectDoesNotExist:
    #         raise forms.ValidationError(u'Invalid user')
    #     else:
    #         return user


class AdminMealForm(forms.ModelForm):
    """ Meal form for the admin interface """
    user = forms.IntegerField(widget=forms.HiddenInput)

    class Meta:
        """ form meta options """
        model = Meal

    class Media:
        """ form media files """
        css = {
            'all': (
                'common/css/admin/admin.css',
                "common/css/chosen.css"
            )
        }
        js = (
            'users/js/rdi_pie_chart.js', 'users/js/jquery-1.8.0.min.js',
            "common/js/chosen.jquery.js",
            'users/js/highcharts/highcharts.js',
            'users/js/draw_charts.js',
             'users/js/meals.js',
            'common/js/hide_user.js'
        )

    def __init__(self, *args, **kwargs):
        """ customizing """
        super(AdminMealForm, self).__init__(*args, **kwargs)
        self.fields['daily_consume'].required = False
        self.fields['daily_consume'].widget = forms.HiddenInput()

    def clean_user(self):
        """ validates that the user exists and returns the User object """
        try:
            user = User.objects.get(id=self.cleaned_data['user'])
        except ObjectDoesNotExist:
            raise forms.ValidationError(u'Invalid user')
        else:
            return user

    def clean(self):
        """
        if there is a daily_consume just returns the cleaned data
        else gets or creates the current daily consume, adds it to the
        cleaned data and returns the whole cleaned data
        """
        if self.cleaned_data.get('daily_consume'):
            return self.cleaned_data
        cleaned_data = self.cleaned_data.copy()
        cleaned_data['daily_consume'], created = Daily_Consume.\
            objects.get_or_create(user=self.cleaned_data['user'],
                                  date=date.today())
        return cleaned_data


class InlineFoodMealForm(forms.ModelForm):
    """ admin inline form for FoodMeal model"""

    class Meta:
        """ meta class definitions """
        model = MealFood

    def __init__(self, *args, **kwars):
        """ customizing fields data """
        super(InlineFoodMealForm, self).__init__(*args, **kwars)
        self.fields['grams'].required = False
        self.fields['food'].empty_label = 'Select a food'
        self.fields['food'].required = False

    def clean_presentation_multiplier(self):
        """ verifies that if the multiplier is set then it's bigger than 0 """
        c_d = super(InlineFoodMealForm, self).clean()
        multiplier = c_d.get('presentation_multiplier', None)
        if multiplier is not None and multiplier <= 0:
            raise forms.ValidationError(
                'The multiplier must be bigger than 0')
        else:
            return multiplier

    def clean(self):
        """ adds validation for grams, presentation and food inputs """
        c_d = super(InlineFoodMealForm, self).clean().copy()
        food = c_d.get('food')
        presentation = c_d.get('presentation')
        grams = c_d.get('grams')
        multiplier = c_d.get('presentation_multiplier')

        if grams and presentation:
            raise forms.ValidationError(
                'Select grams or a presentation, not both')
        elif grams or presentation:
            if food:
                if grams and multiplier:
                    msg =(
                        'Grams has been filled so the multiplier is not '
                        'necessary'
                    )
                    self._errors['presentation_multiplier'] = \
                        self.error_class([msg])
                    del c_d['presentation_multiplier']
                # setting a default value for the presentation
                if not multiplier and presentation:
                    c_d['presentation_multiplier'] = 1
            else:
                msg = 'This field is required.'
                self._errors['food'] = self.error_class([msg])
                del c_d['food']
        else:
            if food:
                raise forms.ValidationError(
                    'Type the grams or select a presentation')

        return c_d

    # def save(self, *args, **kwargs):
    #     """
    #     * if the presentation is selected but not multiplier is typed
    #       then sets it to 1.
    #     * Saves and returns the MealFood object.
    #     """
    #     c_d = super(InlineFoodMealForm, self).clean()
    #     presentation = c_d.get('presentation')
    #     multiplier = c_d.get('presentation_multiplier')

    #     # setting a default multiplier to the presentation
    #     kwargs['commit'] = False
    #     obj = super(InlineFoodMealForm, self).save(*args, **kwargs)
    #     if not multiplier and presentation:
    #         obj.presentation_multiplier = 1
    #     obj.save()

    #     return obj


class AdminAddUserProfileForm(forms.ModelForm):
    """ Addition UserProfile form for the admin interface """
    username = UsernameField(label=_('username'))
    first_name = forms.CharField(label=_('first name'))
    last_name = forms.CharField(label=_('last name'))
    password = forms.CharField(
        label=_('password'), widget=forms.PasswordInput)
    email = forms.EmailField(label=_('email'))

    class Meta:
        """ form meta options """
        model = UserProfile
        exclude = ('bmr', 'tee', 'calories')

    class Media:
        """ form media files """
        css = {
            'all': ('common/css/admin/admin.css', 'admin/css/forms.css',
                    'admin/css/base.css', 'admin/css/widgets.css')
        }
        # needs to include admin/js/core.js before jsi18n in the template
        js = (
            'admin/js/core.js', 'admin/js/jquery.js',
            'admin/js/jquery.init.js', 'common/js/hide_user.js'
        )

    def __init__(self, *args, **kwargs):
        """ customization """
        super(AdminAddUserProfileForm, self).__init__(*args, **kwargs)
        self.fields['user'].widget = forms.HiddenInput()
        self.fields['user'].required = False
        self.fields['birthday'].widget = widgets.AdminDateWidget()
        self.fields['rdi_carbohydrates'].required = False
        self.fields['rdi_protein'].required = False
        self.fields['rdi_fat'].required = False
        self.fields.keyOrder = ['username', 'email', 'user', 'password',
                                'first_name', 'last_name',
                                'gender', 'birthday', 'height', 'weight',
                                'physical_activity', 'goal',
                                'body_fat_percent', 'recalculate_rdis',
                                'rdi_protein',
                                'rdi_carbohydrates', 'rdi_fat']

    def clean(self):
        """ verifies that the username and email are unique """
        c_d = self.cleaned_data
        if 'username' in c_d and User.objects.filter(
                username=c_d['username']).exists():
            raise forms.ValidationError('This username is already registered.')
        if 'email' in c_d and User.objects.filter(email=c_d['email']).exists():
            raise forms.ValidationError('This email is already registered.')
        recalculate = c_d.get('recalculate_rdis', None)
        carbs = c_d.get('rdi_carbohydrates', None)
        protein = c_d.get('rdi_protein', None)
        fat = c_d.get('rdi_fat', None)
        if not recalculate and (not carbs or not protein or not fat):
            raise forms.ValidationError(
                'You must fill all the recommended dietary intake values or '
                'select calculate RDIs to allow the application calculates '
                'them for you.'
            )
            
        return c_d

    @transaction.atomic
    def save(self, request, *args, **kwargs):
        """
        creates the user, then creates and returns the UserProfile object
        """
        c_d = self.cleaned_data.copy()
        user = User.objects.create(username=c_d['username'],
                                   email=c_d['email'],
                                   first_name=c_d['first_name'],
                                   last_name=c_d['last_name'],
                                   is_staff=True)
        user.set_password(c_d['password'])
        user.save()
        userprofile = UserProfile.objects.create(
            user=user, gender=c_d['gender'], birthday=c_d['birthday'],
            height=c_d['height'], weight=c_d['weight'],
            physical_activity=c_d['physical_activity'], goal=c_d['goal'],
            body_fat_percent=c_d['body_fat_percent'],
            recalculate_rdis=c_d['recalculate_rdis'],
            rdi_protein=c_d['rdi_protein'],
            rdi_carbohydrates=c_d['rdi_carbohydrates'], rdi_fat=c_d['rdi_fat']
        )
        messages.success(request, u'The UserProfile was created successfully.')
        if '_addanother' in self.data:
            return redirect('admin:users_userprofile_add')
        if '_save' in self.data:
            return redirect('admin:users_userprofile_changelist')
        else:
            return redirect('admin:users_userprofile_change', userprofile.id)


class AdminUserProfileForm(forms.ModelForm):
    """ UserProfile form for the admin interface """
    username = forms.CharField()
    first_name = forms.CharField()
    last_name = forms.CharField()
    email = forms.EmailField()

    class Meta:
        """ form meta options """
        model = UserProfile

    class Media:
        """ form media files """
        css = {'all': ('common/css/admin/admin.css', )}
        js = ('common/js/hide_user.js', )

    def __init__(self, *args, **kwargs):
        """ customizes user widget """
        super(AdminUserProfileForm, self).__init__(*args, **kwargs)
        self.fields['user'].widget = forms.HiddenInput()

    def clean(self):
        """ verifies that the username and email are unique """
        c_d = self.cleaned_data
        if not 'username' in c_d or not 'email' in c_d:
            return c_d
        if (c_d['username'] == c_d['user'].username) or not (
                User.objects.filter(username=c_d['username']).exists()):
            if (c_d['email'] == c_d['user'].email) or not (
                    User.objects.filter(email=c_d['email']).exists()):
                return c_d
            else:
                raise forms.ValidationError(
                    'This email is already registered.')
        else:
            raise forms.ValidationError('This username is already registered.')

    @transaction.atomic
    def save(self, *args, **kwargs):
        """
        saves the data into the User and Userprofile tables, then returns the
        UserProfile object
        """
        c_d = self.cleaned_data
        user = c_d['user']
        user.username = c_d['username']
        user.email = c_d['email']
        user.first_name = c_d['first_name']
        user.last_name = c_d['last_name']
        user.save()
        return super(AdminUserProfileForm, self).save(*args, **kwargs)
