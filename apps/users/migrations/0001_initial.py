# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'UserProfile'
        db.create_table(u'users_userprofile', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('height', self.gf('django.db.models.fields.FloatField')()),
            ('weight', self.gf('django.db.models.fields.FloatField')()),
            ('physical_activity', self.gf('django.db.models.fields.CharField')(max_length=1)),
            ('goal', self.gf('django.db.models.fields.CharField')(max_length=1)),
            ('gender', self.gf('django.db.models.fields.CharField')(max_length=1)),
            ('birthday', self.gf('django.db.models.fields.DateField')()),
            ('body_fat_percent', self.gf('django.db.models.fields.FloatField')()),
            ('bmr', self.gf('django.db.models.fields.FloatField')()),
            ('tee', self.gf('django.db.models.fields.FloatField')()),
            ('rdi_carbohydrates', self.gf('django.db.models.fields.FloatField')()),
            ('rdi_protein', self.gf('django.db.models.fields.FloatField')()),
            ('rdi_fat', self.gf('django.db.models.fields.FloatField')()),
            ('calories', self.gf('django.db.models.fields.FloatField')()),
            ('user', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['auth.User'], unique=True)),
        ))
        db.send_create_signal(u'users', ['UserProfile'])

        # Adding model 'Daily_Consume'
        db.create_table(u'users_daily_consume', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('carbohydrates', self.gf('django.db.models.fields.FloatField')(default=0)),
            ('protein', self.gf('django.db.models.fields.FloatField')(default=0)),
            ('fat', self.gf('django.db.models.fields.FloatField')(default=0)),
            ('calories', self.gf('django.db.models.fields.FloatField')(default=0)),
            ('date', self.gf('django.db.models.fields.DateField')(default=datetime.date.today)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'])),
        ))
        db.send_create_signal(u'users', ['Daily_Consume'])

        # Adding model 'Meal'
        db.create_table(u'users_meal', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('carbohydrates', self.gf('django.db.models.fields.FloatField')(default=0)),
            ('protein', self.gf('django.db.models.fields.FloatField')(default=0)),
            ('fat', self.gf('django.db.models.fields.FloatField')(default=0)),
            ('calories', self.gf('django.db.models.fields.FloatField')(default=0)),
            ('time', self.gf('django.db.models.fields.TimeField')(default=datetime.time(14, 1, 59, 375555))),
            ('daily_consume', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['users.Daily_Consume'])),
        ))
        db.send_create_signal(u'users', ['Meal'])

        # Adding model 'MealFood'
        db.create_table(u'users_mealfood', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('meal', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['users.Meal'])),
            ('food', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['foods.Food'])),
            ('grams', self.gf('django.db.models.fields.FloatField')(null=True, blank=True)),
            ('presentation', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['foods.Presentation'], null=True, blank=True)),
        ))
        db.send_create_signal(u'users', ['MealFood'])


    def backwards(self, orm):
        # Deleting model 'UserProfile'
        db.delete_table(u'users_userprofile')

        # Deleting model 'Daily_Consume'
        db.delete_table(u'users_daily_consume')

        # Deleting model 'Meal'
        db.delete_table(u'users_meal')

        # Deleting model 'MealFood'
        db.delete_table(u'users_mealfood')


    models = {
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Group']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Permission']"}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'foods.food': {
            'Meta': {'ordering': "['name']", 'object_name': 'Food'},
            'calories': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'carbohydrates': ('django.db.models.fields.FloatField', [], {}),
            'fat': ('django.db.models.fields.FloatField', [], {}),
            'grams': ('django.db.models.fields.FloatField', [], {'default': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '120'}),
            'protein': ('django.db.models.fields.FloatField', [], {})
        },
        u'foods.presentation': {
            'Meta': {'object_name': 'Presentation'},
            'food': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['foods.Food']"}),
            'grams': ('django.db.models.fields.FloatField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '120'})
        },
        u'users.daily_consume': {
            'Meta': {'ordering': "['-date']", 'object_name': 'Daily_Consume'},
            'calories': ('django.db.models.fields.FloatField', [], {'default': '0'}),
            'carbohydrates': ('django.db.models.fields.FloatField', [], {'default': '0'}),
            'date': ('django.db.models.fields.DateField', [], {'default': 'datetime.date.today'}),
            'fat': ('django.db.models.fields.FloatField', [], {'default': '0'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'protein': ('django.db.models.fields.FloatField', [], {'default': '0'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']"})
        },
        u'users.meal': {
            'Meta': {'object_name': 'Meal'},
            'calories': ('django.db.models.fields.FloatField', [], {'default': '0'}),
            'carbohydrates': ('django.db.models.fields.FloatField', [], {'default': '0'}),
            'daily_consume': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['users.Daily_Consume']"}),
            'fat': ('django.db.models.fields.FloatField', [], {'default': '0'}),
            'foods': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['foods.Food']", 'through': u"orm['users.MealFood']", 'symmetrical': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'protein': ('django.db.models.fields.FloatField', [], {'default': '0'}),
            'time': ('django.db.models.fields.TimeField', [], {'default': 'datetime.time(14, 1, 59, 384756)'})
        },
        u'users.mealfood': {
            'Meta': {'object_name': 'MealFood'},
            'food': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['foods.Food']"}),
            'grams': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'meal': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['users.Meal']"}),
            'presentation': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['foods.Presentation']", 'null': 'True', 'blank': 'True'})
        },
        u'users.userprofile': {
            'Meta': {'ordering': "['user__last_name']", 'object_name': 'UserProfile'},
            'birthday': ('django.db.models.fields.DateField', [], {}),
            'bmr': ('django.db.models.fields.FloatField', [], {}),
            'body_fat_percent': ('django.db.models.fields.FloatField', [], {}),
            'calories': ('django.db.models.fields.FloatField', [], {}),
            'gender': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            'goal': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            'height': ('django.db.models.fields.FloatField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'physical_activity': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            'rdi_carbohydrates': ('django.db.models.fields.FloatField', [], {}),
            'rdi_fat': ('django.db.models.fields.FloatField', [], {}),
            'rdi_protein': ('django.db.models.fields.FloatField', [], {}),
            'tee': ('django.db.models.fields.FloatField', [], {}),
            'user': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['auth.User']", 'unique': 'True'}),
            'weight': ('django.db.models.fields.FloatField', [], {})
        }
    }

    complete_apps = ['users']