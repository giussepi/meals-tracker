//////////////////////////////////////////////////////////////////////////////
// 
// Draws in the container1:
//   * A column chart of the  intake (proteins, carbohydrates and fat).
//   * A line chart with the RDIs.
//   * A pie chart with the  intake.
// Draws in the container2:
//   * A column chart with the cals (calories).
// Hides Daily consume row
//
//////////////////////////////////////////////////////////////////////////////
(function($) {
  $(document).ready(function(){

    var chart1;
    var chart2;
    daily_intake_track = function(intake, rdis, cals, container1, container2){

      chart1 = new Highcharts.Chart({
        chart: {
	  renderTo: container1,
	  type: 'column'
        },
        title: {
	  text: 'Total intake of the day'
        },
	subtitle: {
	  text: '(RDI = Recommended Dietary Intake)'
	},
        xAxis: {
	  categories: ['Proteins', 'Carbohydrates ', 'Fat']
        },
        yAxis: {
	  title: {
	    text: 'Grams'
	  }
        },
        labels: {
          items: [{
            html: 'Total consumption',
            style: {
              left: '12px',
              top: '8px',
              color: 'black'
            }
          }]
        },
        series: [{
	  name: "Day's intake",
	  data: intake
        },{
	  type: 'spline',
	  name: 'RDI',
	  data: rdis,
	  marker: {
            lineWidth: 2,
            lineColor: Highcharts.getOptions().colors[3],
            fillColor: 'white'
	  }
	},{
          type: 'pie',
	  allowPointSelect: true,
	  cursor: 'pointer',
          name: "Day's intake",
          data: [{
            name: 'Proteins',
            y: intake[0],
            color: '#4572A7'
          }, {
            name: 'Carbohydrates',
            y: intake[1],
            color: '#AA4643'
          }, {
            name: 'Fat',
            y: intake[2],
            color: '#89A54E'
          }],
          center: [65, 80],
          size: 100,
          showInLegend: false,
          dataLabels: {
            enabled: false
          }
	}]
      });

      chart2 = new Highcharts.Chart({
        chart: {
	  renderTo: container2,
	  type: 'column'
        },
        title: {
	  text: 'Total Calories of the day'
        },
	subtitle: {
	  text: '(RDI = Recommended Dietary Intake)'
	},
        xAxis: {
	  categories: ['calories']
        },
        yAxis: {
	  title: {
	    text: 'Kcal'
	  }
        },
        series: [{
	  name: "Caloric intake",
	  data: [cals[1]]
        },{
	  name: "RDI calories",
	  data: [cals[0]]
	}]
      });
    };

    // Hiding daily consume
    $('div.form-row.field-daily_consume').hide();

  });
})(jQuery);
