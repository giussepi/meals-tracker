//////////////////////////////////////////////////////////////////////////////
// 
// Shows initial presentation's options properly filtered
// Updates presentation dropdown when a food is selected
// Initializes chosen drop downs
//
//////////////////////////////////////////////////////////////////////////////
(function($) {
  $(document).ready(function(){

    // jquery function to update the presentation based on the food selected
    $.fn.update_presentation = function(){
      var $presentation_select = $(this).closest('td')
        .siblings('.field-presentation').find('select');
      var value = $(this).val();
      if (value) {
        $.ajax({
          url: '/foods/json_presentations_by_food/' + value  + '/',
          type: "GET",
          datatype: "json",
          error: function(jqXHR, textStatus, errorThrown){
            console.log(errorThrown);
          },
          success: function(data, textStatus, jqXHR){
            var response = JSON.parse(data);
            $presentation_select.empty().append(response.options);
          }
        });
      }
    };

    // Showing initial presentation's options properly filtered
    $.each($('td.field-food select'), function(key, value){
      var $presentation_select = $(this).closest('td')
        .siblings('.field-presentation').find('select');
      var mealfood = $(this).closest('td')
        .siblings('.original').find('input')[0];
      var value = $(mealfood).val();
      if (value){
        $.ajax({
          url: '/users/json_presentations_by_mealfood/' + value + '/',
          type: "GET",
          datatype: "json",
          error: function(jqXHR, textStatus, errorThrown){
            console.log(errorThrown);
          },
          success: function(data, textStatus, jqXHR){
            var response = JSON.parse(data);
            $presentation_select.empty().append(response.options);
          }
        });
      } else if ($(this).val()) {
        $(this).update_presentation();
      } else{
        var options = $presentation_select.children();
        var default_option = options[0];
        options.remove();
        $presentation_select.append(default_option.outerHTML);
      }
    });

    // Updating presentation dropdown when a food is selected
    $(document).on('change', 'td.field-food select', function(){
      $(this).update_presentation();
    });

    // Initialiazing chosen dropdowns
    var chosen_handler = function(){
      $('td.field-food select')
        .not('select#id_mealfood_set-__prefix__-food').chosen();
    };
    chosen_handler();
    $('tr.add-row a').click(chosen_handler);

  });
})(jQuery);
