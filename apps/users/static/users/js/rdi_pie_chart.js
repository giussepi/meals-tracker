//////////////////////////////////////////////////////////////////////////////
// 
// Draws in the container1:
//   * A pie chart of the RDI values (proteins, carbohydrates and fat).
//   * if no title is provided sets it to 'Recommended Dietary Intake'
//
//////////////////////////////////////////////////////////////////////////////
(function($) {
    var chart1;
    rdi_pie_chart = function(rdis, container1, title_text){
	if (typeof(title_text) == "undefined") {
	    title_text = 'Recommended Dietary Intake'
	}
	chart1 = new Highcharts.Chart({
            chart: {
		renderTo: container1,
            },
            title: {
		text: title_text
            },
            tooltip: {
        	    pointFormat: '{series.name}: <b>{point.y}g</b>',
            	percentageDecimals: 2
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true
                    },
                    showInLegend: false
                }
            },
            series: [{
                type: 'pie',
                name: "Day's intake",
                data: [{
                    name: 'Proteins',
                    y: rdis[0],
                    color: '#4572A7',
		    sliced: true,
		    selected: true,
                }, {
                    name: 'Carbohydrates',
                    y: rdis[1],
                    color: '#AA4643'
                }, {
                    name: 'Fat',
                    y: rdis[2],
                    color: '#89A54E'
                }],
	    }]
	});

    };
})(jQuery);