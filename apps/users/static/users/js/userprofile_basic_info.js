//////////////////////////////////////////////////////////////////////////////
// 
//  Sets the user information into its corresponding inputs
//
//////////////////////////////////////////////////////////////////////////////
(function($) {
    initial_information = function(user_id, username, email, first_name,
				   last_name){
	$('input#id_user').val(user_id);
	$('input#id_username').val(username);
	$('input#id_email').val(email);
	$('input#id_first_name').val(first_name);
	$('input#id_last_name').val(last_name);
    };
})(jQuery);