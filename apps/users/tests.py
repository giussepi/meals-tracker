# -*- coding: utf-8 -*-
""" users application tests """

from django.test import TestCase
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse
from common.utils import random_string_with_length
from foods.models import Food
from users.models import UserProfile, Daily_Consume, Meal
from datetime import date, datetime, timedelta
from random import randint, sample


class BaseOperation(TestCase):
    """ basic operations """

    def _create_user(self, active=False, superuser=False):
        """ creates a user and his profile, then returns the user """
        u = User.objects.create(
            username=random_string_with_length(5),
            is_active=active, is_staff=True, is_superuser=superuser)
        u.set_password('1')
        u.save()
        UserProfile.objects.create(
            user=u, height=1.7, weight=80.2, physical_activity=u'moderate',
            goal=u'maintain', gender='M', birthday=date.today(),
            rdi_carbohydrates=305, rdi_protein=194, rdi_fat=73, calories=2600
        )
        return u

    def _create_daily_consume(self, user):
        """ creates and returns a new daily consume """
        return Daily_Consume.objects.create(date=date.today(), user=user)

    def _create_foods(self, num=10):
        """ creates the specified number of foods """
        for i in xrange(num):
            Food.objects.create(
                name=random_string_with_length(5), grams=randint(1, 100),
                carbohydrates=randint(1, 100), protein=randint(1, 100),
                fat=randint(1, 100))

    def _create_meal(self, daily_consume):
        """ creates and returns a meal """
        meal = Meal.objects.create(
            time=datetime.now().time(), daily_consume=daily_consume)
        foods = sample(Food.objects.all(), randint(1, 5))
        for food in foods:
            meal.mealfood_set.create(meal=meal, food=food,
                                     grams=randint(1, 200))
        return meal


class Daily_Consume_Admin_Views(BaseOperation):
    """ tests of Daily_Consume admin views """

    def test_all_admin_views(self):
        """ Verifies that all admin User views work good """
        user = self._create_user(True, True)
        result = self.client.login(username=user.username, password='1')
        self.assertEqual(result, True, 'Login process Failed')
        d_consume = self._create_daily_consume(user)

        views_to_test = (
            (reverse('admin:users_daily_consume_changelist'),
             'users_daily_consume_changelist'),
            (reverse('admin:users_daily_consume_add'),
             'user_daily_consume_add'),
            (reverse('admin:users_daily_consume_change',
                     args=(d_consume.id, )),
             'user_daily_consume_change'),
        )

        for view in views_to_test:
            response = self.client.get(view[0])
            self.assertEqual(response.status_code, 200, str(
                response.status_code) + ' ' + view[1])

    def test_add(self):
        """
        verifies that a new Daily_Consume object is created using the
        specified form
        """
        user = self._create_user(True, True)
        result = self.client.login(username=user.username, password='1')
        self.assertEqual(result, True, 'Login process Failed')
        self.assertEqual(Daily_Consume.objects.count(), 0,
                         'A Daily_Consume object was already created.')
        response = self.client.post(reverse('admin:users_daily_consume_add'),
                                    {'date': date.today(), 'user': user.id})
        self.assertEqual(response.status_code, 302, str(response.status_code))
        self.assertEqual(Daily_Consume.objects.count(), 1,
                         'The Daily_Consume object was not created.')

    def test_change(self):
        """ verifies that the Daily_Consume updates are performed correctly """
        user = self._create_user(True, True)
        d_consume = self._create_daily_consume(user)
        result = self.client.login(username=user.username, password='1')
        self.assertEqual(result, True, 'Login process Failed')

        # changing the date of the Daily_Consume object
        new_date = date.today() + timedelta(1)
        response = self.client.post(
            reverse('admin:users_daily_consume_change', args=(d_consume.id, )),
            {'date': new_date, 'user': user.id})
        self.assertEqual(response.status_code, 302, str(response.status_code))
        d_consume = Daily_Consume.objects.get(id=d_consume.id)
        self.assertEqual(d_consume.date, new_date,
                         'The daily_consume data was not modified')

        # creating meals
        self._create_foods()
        for i in xrange(5):
            self._create_meal(d_consume)
        prots = 0
        carbs = 0
        fats = 0
        cals = 0
        for meal in d_consume.meal_set.all():
            prots += meal.protein
            carbs += meal.carbohydrates
            fats += meal.fat
            cals += meal.calories
        updated_d_consume = Daily_Consume.objects.get(id=1)
        self.assertTrue((updated_d_consume.protein == prots and
                         updated_d_consume.carbohydrates == carbs and
                         updated_d_consume.fat == fats and
                         updated_d_consume.calories == cals),
                        'The daily consume were not updated correctly')

        # modifying meals
        meal1 = Meal.objects.get(id=1)
        mealfood = meal1.mealfood_set.all()[0]
        mealfood.delete()
        last_meal = Meal.objects.latest('id')
        for mealfood in last_meal.mealfood_set.all():
            mealfood.grams += 10
            mealfood.save()
        prots = 0
        carbs = 0
        fats = 0
        cals = 0
        for meal in updated_d_consume.meal_set.all():
            prots += meal.protein
            carbs += meal.carbohydrates
            fats += meal.fat
            cals += meal.calories
        last_d_consume = Daily_Consume.objects.get(id=1)
        self.assertTrue(
            (last_d_consume.protein != updated_d_consume.protein and
             last_d_consume.carbohydrates != updated_d_consume.carbohydrates
             and last_d_consume.fat != updated_d_consume.fat and
             last_d_consume.calories != updated_d_consume.calories),
            'The daily consume were not updated correctly')
        self.assertTrue((last_d_consume.protein == prots and
                         last_d_consume.carbohydrates == carbs and
                         last_d_consume.fat == fats and
                         last_d_consume.calories == cals),
                        'The daily consume were not updated correctly')


class Meals_Admin_Views(BaseOperation):
    """ tests of Meals admin views  """

    def test_all_admin_views(self):
        """ Verifies that all admin Meals views work good """
        user = self._create_user(True, True)
        result = self.client.login(username=user.username, password='1')
        self.assertEqual(result, True, 'Login process Failed')
        d_consume = self._create_daily_consume(user)
        self._create_foods()
        meal = self._create_meal(d_consume)

        views_to_test = (
            (reverse('admin:users_meal_changelist'),
             'users_meal_changelist'),
            (reverse('admin:users_meal_add'),
             'user_meal_add'),
            (reverse('admin:users_meal_change',
                     args=(meal.id, )),
             'user_meal_change'),
        )

        for view in views_to_test:
            response = self.client.get(view[0])
            self.assertEqual(response.status_code, 200, str(
                response.status_code) + ' ' + view[1])

    def test_add(self):
        """
        verifies that a new Meal object is created using the specified form
        and the values calculated are correct.
        """
        user = self._create_user(True, True)
        self._create_daily_consume(user)
        self._create_foods()
        foods = sample(Food.objects.all(), 2)
        result = self.client.login(username=user.username, password='1')
        self.assertEqual(result, True, 'Login process Failed')
        self.assertEqual(Meal.objects.count(), 0,
                         'A Meal object was already created.')

        # creating a meal with 2 foods
        response = self.client.post(
            reverse('admin:users_meal_add'),
            {'time': datetime.now().time().strftime('%H:%M:%S'),
             'user': user.id,
             'mealfood_set-TOTAL_FORMS': 3, 'mealfood_set-INITIAL_FORMS': 0,
             'mealfood_set-0-food': foods[0].id, 'mealfood_set-0-grams': 100,
             'mealfood_set-1-food': foods[1].id, 'mealfood_set-1-grams': 100})
        self.assertEqual(response.status_code, 302, str(response.status_code))
        self.assertEqual(
            Meal.objects.count(), 1, 'The Meal object was not created.')
        meal = Meal.objects.get(id=1)
        self.assertEqual(meal.foods.count(), 2,
                         'The foods were no added to the meal.')

        # calculating the macronutrients values of the meal
        prot = 0
        carbs = 0
        fat = 0
        cals = 0
        tmp = 0
        for food in foods:
            tmp = 100 / food.grams
            prot += tmp * food.protein
            carbs += tmp * food.carbohydrates
            fat += tmp * food.fat
            cals += tmp * food.calories
        self.assertEqual(
            (meal.protein, meal.carbohydrates, meal.fat, meal.calories),
            (prot, carbs, fat, cals),
            'The macronutrients values were not calculated correctly')

    def test_change(self):
        """ verifies that the Meal updates are performed correctly """
        user = self._create_user(True, True)
        d_consume = self._create_daily_consume(user)
        self._create_foods()
        meal = self._create_meal(d_consume)

        # increasing the grams of food by 20 grams
        for mealfood in meal.mealfood_set.all():
            mealfood.grams += 20
            mealfood.save()
        updated_meal = Meal.objects.get(id=1)
        self.assertTrue(
            (meal.protein < updated_meal.protein and
             meal.carbohydrates < updated_meal.carbohydrates and
             meal.fat < updated_meal.fat and
             meal.calories < updated_meal.calories),
            'The macronutrients values were no recalculated correctly.')

        # calculating the macronutrients values of the meal
        prot = 0
        carbs = 0
        fat = 0
        cals = 0
        tmp = 0
        for mealfood in updated_meal.mealfood_set.all():
            tmp = mealfood.grams / mealfood.food.grams
            prot += tmp * mealfood.food.protein
            carbs += tmp * mealfood.food.carbohydrates
            fat += tmp * mealfood.food.fat
            cals += tmp * mealfood.food.calories
        self.assertEqual(
            (updated_meal.protein, updated_meal.carbohydrates,
             updated_meal.fat, updated_meal.calories),
            (prot, carbs, fat, cals),
            'The macronutrients values were not recalculated correctly')


class UserProfile_Admin_Views(BaseOperation):
    """ tests of UserProfile admin views  """

    def test_all_admin_views(self):
        """ Verifies that all admin UserProfile views work good """
        user = self._create_user(True, True)
        result = self.client.login(username=user.username, password='1')
        self.assertEqual(result, True, 'Login process Failed')

        views_to_test = (
            (reverse('admin:users_userprofile_changelist'),
             'users_userprofile_changelist'),
            (reverse('admin:users_userprofile_add'),
             'user_userprofile_add'),
            (reverse('admin:users_userprofile_change',
                     args=(user.userprofile.id, )),
             'user_userprofile_change'),
        )

        for view in views_to_test:
            response = self.client.get(view[0])
            self.assertEqual(response.status_code, 200, str(
                response.status_code) + ' ' + view[1])

    def test_add(self):
        """ verifies that the UserProfile creation is done correctly """
        user = self._create_user(True, True)
        result = self.client.login(username=user.username, password='1')
        self.assertEqual(result, True, 'Login process Failed')

        # creating the UserProfile
        response = self.client.post(
            reverse('admin:users_userprofile_add'),
            {'username': 'jhon', 'email': 'jhon@mail.com', 'password': '12345',
             'first_name': 'jhon', 'last_name': 'connor', 'gender': u'M',
             'birthday': date.today(), 'height': 1.8, 'weight': 80,
             'physical_activity': u'moderate', 'goal': u'maintain',
             'rdi_carbohydrates': 305, 'rdi_protein': 194, 'rdi_fat': 73,
             'calories': 2600})
        self.assertEqual(response.status_code, 302, str(response.status_code))
        self.assertTrue(
            (UserProfile.objects.count() == 2 and User.objects.count() == 2),
            'The user and/or the profile were not created.')
        self.assertTrue(
            User.objects.get(id=2).userprofile == UserProfile.objects.
            get(id=2),
            'The user and the profiles created were not associated')

    def test_change(self):
        """
        verifies that the extra context is complete and that the User and
        UserProfile are updated correctly.
        """
        user = self._create_user(True, True)
        result = self.client.login(username=user.username, password='1')
        self.assertEqual(result, True, 'Login process Failed')

        # verifying extra context
        response = self.client.get(
            reverse('admin:users_userprofile_change',
                    args=(user.userprofile.id, )))
        self.assertEqual(response.status_code, 200, str(response.status_code))
        self.assertTrue(('profile_user' in response.context and
                         'rdis' in response.context),
                        'Not all the required extra context was passed.')
        self._create_daily_consume(user)
        response = self.client.get(
            reverse('admin:users_userprofile_change',
                    args=(user.userprofile.id, )))
        self.assertEqual(response.status_code, 200, str(response.status_code))
        self.assertTrue(('profile_user' in response.context and
                         'rdis' in response.context and
                         'weekly_chart' in response.context),
                        'Not all the required extra context was passed.')

        # using the form to modify data
        new_date = date.today() + timedelta(1)
        response = self.client.post(
            reverse('admin:users_userprofile_change',
                    args=(user.userprofile.id, )),
            {'username': 'jhon', 'email': 'jhon@mail.com', 'user': user.id,
             'first_name': 'jhon', 'last_name': 'connor', 'gender': u'M',
             'birthday': new_date, 'height': 2, 'weight': 100,
             'physical_activity': u'high', 'goal': u'lose',
             'rdi_carbohydrates': 250, 'rdi_protein': 200, 'rdi_fat': 60,
             'calories': 1200})
        self.assertEqual(response.status_code, 302, str(response.status_code))
        user = User.objects.get(id=user.id)
        profile = user.get_profile()
        self.assertTrue(
            (user.username == 'jhon' and user.email == 'jhon@mail.com' and
             user.first_name == 'jhon' and user.last_name == 'connor'),
            'The user was no updated correctly')
        self.assertTrue(
            (profile.birthday == new_date and profile.height == 2 and
             profile.weight == 100 and profile.physical_activity == u'high' and
             profile.goal == u'lose' and profile.rdi_carbohydrates == 250 and
             profile.rdi_protein == 200 and profile.rdi_fat == 60),
            'The profile was no updated correctly')
