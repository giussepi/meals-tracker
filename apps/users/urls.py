# -*- coding: utf-8 -*-
""" users' urls """

from django.conf.urls import patterns, url


urlpatterns = patterns(
    'users.views',
    url(
        r'^json_presentations_by_mealfood/(?P<mealfood_id>\d+)/$',
        'json_presentations_by_mealfood',
        name='users.json_presentations_by_mealfood'
    ),
)
