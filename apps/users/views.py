# -*- coding: utf-8 -*-
""" users' views """

import json

from django.http import HttpResponse, HttpResponseBadRequest
from django.shortcuts import get_object_or_404

from .models import MealFood


def json_presentations_by_mealfood(request, mealfood_id):
    """ returns all the mealfood's presentation in json format """
    if request.is_ajax() and request.method == 'GET':
        mealfood = get_object_or_404(MealFood, pk=mealfood_id)
        options = mealfood.get_html_options_presentations()
        return HttpResponse(json.dumps({'options': options}))
    return HttpResponseBadRequest()
