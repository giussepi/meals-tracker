# -*- coding: utf-8 -*-
""" local settings """

from .base import *


ADMINS = (
    ('Giussepi', 'giussepexy@gmail.com'),
)

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'bodybuilding',
        'USER': 'postgres',
        'PASSWORD': '12345',
        'HOST': 'localhost',
        'PORT': '',
    },
}
