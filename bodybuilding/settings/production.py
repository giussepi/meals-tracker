# -*- coding: utf-8 -*-
""" production server settings """

from .base import *

ENABLE_SSL = True
DEBUG = False
TEMPLATE_DEBUG = DEBUG
MODELTRANSLATION_DEBUG = DEBUG

ALLOWED_HOSTS += [
    'mydomain.com',
    'www.mydomain.com',
    os.environ['MEALSTRACKER_ALLOWED_IP_1'],
    os.environ['MEALSTRACKER_ALLOWED_IP_2'],
    os.environ['MEALSTRACKER_ALLOWED_IP_3']
]

SECRET_KEY = os.environ['MEALSTRACKER_APP_SECRET_KEY']

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': os.environ['MEALSTRACKER_DB_NAME'],
        'USER': os.environ['MEALSTRACKER_DB_USER'],
        'PASSWORD': os.environ['MEALSTRACKER_DB_PASSWORD'],
        'HOST': os.environ['MEALSTRACKER_DB_HOST'],
        'PORT': os.environ['MEALSTRACKER_DB_PORT'],
    },
}
