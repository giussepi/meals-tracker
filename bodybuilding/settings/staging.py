# -*- coding: utf-8 -*-
""" staging server settings """

from .base import *

ENABLE_SSL = True
DEBUG = False
TEMPLATE_DEBUG = DEBUG
MODELTRANSLATION_DEBUG = DEBUG

ALLOWED_HOSTS += ['173.0.54.119']

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'bodybuilding',
        'USER': 'postgres',
        'PASSWORD': '12345',
        'HOST': 'localhost',
        'PORT': '',
    },
}
